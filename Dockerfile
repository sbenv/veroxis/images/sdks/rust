FROM registry.gitlab.com/sbenv/veroxis/images/cargo-zigbuild:0.19.1 as cargo-zigbuild
FROM registry.gitlab.com/sbenv/veroxis/images/cargo-edit:0.12.3 as cargo-edit
FROM registry.gitlab.com/sbenv/veroxis/images/cargo-sccache:0.8.1 as cargo-sccache
FROM registry.gitlab.com/sbenv/veroxis/images/cargo-nextest:0.9.47 as cargo-nextest
FROM registry.gitlab.com/sbenv/veroxis/images/sdks/zig:0.13.0 as zig

# ---

FROM registry.gitlab.com/sbenv/veroxis/images/rust:1.80.1-debian

RUN set -eux && \
    export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get upgrade --yes && \
    apt-get install --yes "meson" "cmake" "jq" && \
    apt-get install --yes "alsa-utils" "libudev-dev" "libasound2-dev" && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/{cache,log,debconf,apt} "/usr/share/doc" && \
    apt-get clean

RUN set -eux && \
    rustup component add "rustfmt" && \
    rustup component add "clippy" && \
    chmod -R "g+w,o+w" "${CARGO_HOME}" && \
    chmod -R "g+w,o+w" "${RUSTUP_HOME}"

COPY --from=zig "/usr/bin/zig" "/usr/bin/zig"
COPY --from=zig "/usr/lib/zig" "/usr/lib/zig"
RUN chmod -R "g+w,o+w" "/usr/lib/zig"

COPY --from=cargo-sccache "/usr/local/bin/sccache" "/usr/local/bin/sccache"

COPY --from=cargo-edit "/usr/local/bin/cargo-upgrade" "/usr/local/bin/cargo-upgrade"
COPY --from=cargo-edit "/usr/local/bin/cargo-set-version" "/usr/local/bin/cargo-set-version"

COPY --from=cargo-nextest "/usr/local/bin/cargo-nextest" "/usr/local/bin/cargo-nextest"

COPY --from=cargo-zigbuild "/usr/local/bin/cargo-zigbuild" "/usr/local/bin/cargo-zigbuild"
