ARG RUST_VERSION

FROM registry.gitlab.com/sbenv/veroxis/images/macosx-sdks:13.0 as macosx-sdk

FROM registry.gitlab.com/sbenv/veroxis/images/sdks/rust:${RUST_VERSION}

ARG IMAGE_ARCH
ARG TARGET_ARCH

RUN set -eux && \
    rustup toolchain add "nightly" && \
    rustup target add "${TARGET_ARCH}-apple-darwin" --toolchain="${RUST_VERSION}" && \
    rustup target add "${TARGET_ARCH}-apple-darwin" --toolchain="nightly" && \
    rustup component add "rust-src" --toolchain="nightly" && \
    rustup component add "rustfmt" --toolchain="nightly" && \
    rustup component add "clippy" --toolchain="nightly" && \
    chmod -R "g+w,o+w" "${CARGO_HOME}" && \
    chmod -R "g+w,o+w" "${RUSTUP_HOME}"

COPY --from=macosx-sdk "/opt/MacOSX.sdk" "/opt/MacOSX.sdk"
ENV SDKROOT="/opt/MacOSX.sdk"
