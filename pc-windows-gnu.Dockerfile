ARG RUST_VERSION

FROM registry.gitlab.com/sbenv/veroxis/images/sdks/rust:${RUST_VERSION}

ARG IMAGE_ARCH
ARG TARGET_ARCH

RUN set -eux && \
    apt-get update && \
    apt-get install --yes "gcc-mingw-w64-x86-64" && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/{cache,log,debconf,apt} "/usr/share/doc" && \
    apt-get clean && \
    rustup toolchain add "nightly" && \
    rustup target add "${TARGET_ARCH}-pc-windows-gnu" --toolchain="${RUST_VERSION}" && \
    rustup target add "${TARGET_ARCH}-pc-windows-gnu" --toolchain="nightly" && \
    rustup component add "rust-src" --toolchain="nightly" && \
    rustup component add "rustfmt" --toolchain="nightly" && \
    rustup component add "clippy" --toolchain="nightly" && \
    chmod -R "g+w,o+w" "${CARGO_HOME}" && \
    chmod -R "g+w,o+w" "${RUSTUP_HOME}"
